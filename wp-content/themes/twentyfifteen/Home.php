<?php
/**
	* Template Name: home
	*/
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<div class="col-lg-12"><br><br></div>
<div class="col-lg-12 col-xs-12 col-md-12 padding">
	  <!--  Demos -->
   <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src="<?php echo get_template_directory_uri();?>/Images/Slideimg.jpg"  style="width:100%;">
      </div>

      <div class="item">
        <img src="<?php echo get_template_directory_uri();?>/Images/Slideimg.jpg"  style="width:100%;">
      </div>
  
      <div class="item">
        <img src="<?php echo get_template_directory_uri();?>/Images/Slideimg.jpg"  style="width:100%;">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>

<div class="col-lg-12 col-xs-12 col-md-12 padding Part_Companies">
	<div class="container">
		<div class="col-lg-12 col-xs-12 titles_company">
		<span>شركات </span>
			<span style="color: blue;">المجموعه</span>
		</div>
		<div class="col-lg-12 col-xs-12 col-md-12 imgsForCompanies">
			<div class="col-lg-4 col-xs-12 col-md-4 OneItem float_right" style="background-image: url('<?php echo get_template_directory_uri();?>/Images/logo-1.png');">
				
			</div>
			<div class="col-lg-4 col-xs-12 col-md-4 OneItem float_right" style="background-image: url('<?php echo get_template_directory_uri();?>/Images/logo-2.png');">
				
			</div>
			<div class="col-lg-4 col-xs-12 col-md-4 OneItem float_right" style="background-image: url('<?php echo get_template_directory_uri();?>/Images/logo-3.png');">
				
			</div>
		</div>
	</div>
</div>
<div class="col-lg-12"><br></div>
<div class="col-lg-12 col-xs-12 col-md-12 padding El2ola">
<div class="container">
<div class="col-lg-12 col-xs-12 col-md-12">
<div class="col-lg-6 col-xs-6 col-md-6 float_right">
<div class="col-lg-12 col-xs-12 titles_company">
		<span>الاولى  </span>
			<span style="color: blue;">التمويل العقارى</span>
		</div>


<div class="col-lg-12 col-xs-12 col-md-12 company_detail">
<p >
	أولى شركـــات التمويل العقاري بجمهورية مصر العربية. تم تأسيسها في أغسطس عام 2003 كشركة مساهمة مصرية، وبدأت ممارسة النشاط الفعلي في فبراير 2004 طبقا لقانون التمويل العقاري رقم 148 لسنة 2001 ولائحتها التنفيذية.

تقوم الشركة بتمويل شراء الوحدات السكنية والإدارية والتجارية والفيلات في أي مكان بجمهورية مصر العربية بأقساط شهرية ثابتة تصل إلى 20 سنة وبقيمة تمويل تصل إلى 80% من قيمة الوحدة.

نحن نقدم خدمات التأجير التمويلي والاستثمار والتمويل العقاري في السوق المصري. وقد تم اختيار شركتنا باعتبارها أفضل شركة للتمويل العقاري في مصر لمدة 3 سنوات على التوالي.
</p>
<button class="button btn ComBtn">
	قسط وحدتك الان
</button>
	
</div>

</div>
<div class="col-lg-6 col-xs-6 col-md-6 float_right" >
<img src="<?php echo get_template_directory_uri();?>/Images/image.png">

</div>

</div>
</div>

</div>
<footer>
<div class="col-lg-12 col-xs-12 col-md-12 padding Footer_company">
<div class="col-lg-12 col-xs-12 col-md-12 First_part">
<div class="container">
<div class="col-lg-12 col-md-12 col-xs-12 ">
<div class="col-lg-6 col-md-6 col-xs-6 float_right">
<span class="">
	<img src="<?php echo get_template_directory_uri();?>/Images/footer-txt.png">
</span>
<span class="Text_footer" >
	 اشترك في نشرتنا الإلكترونية لتتابع أخبارنا
</span>
	
	</div>
	<div class="col-lg-6 col-md-6 col-xs-6 float_right">
	<div class="col-lg-12 col-md-12 col-xs-12">
	   <div class="col-lg-6 col-xs-6 float_right"><input class="form_control" type="email" name="mail" style="width: 100%; height: 33px;"> </div>
	  <button class="btn button tasgel">

	  	تسجيل 
	  </button>

	</div>
	</div>
</div>
	
</div>

</div>
<div class="col-lg-12 col-xs-12 col-md-12 second_part">
<div class="container">
<div class="col-lg-12 col-xs-12 col-md-12 Logo_footer_about_us">
<div class="col-lg-3 col-xs-3 col-md-3 float_right">
<img src="../TechnicalTask/Images/footerLogo.png">

</div>
<div class="col-lg-6 col-xs-6 col-md-6 text_footer">
<ul>
	<li>
		<span>
			الرئيسيه 
		</span>
		<span>
			<img src="../TechnicalTask/Images/line_top">
		</span>

	</li>
</ul>
</div>

</div>
</div>

</div>

</div>
	</div><!-- .content-area -->

<?php get_footer(); ?>
