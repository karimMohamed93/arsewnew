<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

	</div><!-- .site-content -->

	<footer class="col-lg-12 col-xs-12">
<div class="col-lg-12 col-xs-12 col-md-12 padding Footer_company">
<div class="col-lg-12 col-xs-12 col-md-12 First_part">
<div class="container">
<div class="col-lg-12 col-md-12 col-xs-12 ">
<div class="col-lg-7 col-md-7 col-xs-7 float_right">
<span class="">
	<img class="imgtxtfooter" src="<?php echo get_template_directory_uri();?>/Images/footer-txt.png">
</span>
<span class="Text_footer" >
	 اشترك في نشرتنا الإلكترونية لتتابع أخبارنا
</span>
	
	</div>
	<div class="col-lg-5 col-md-5 col-xs-5 float_right">
	<div class="col-lg-12 col-md-12 col-xs-12">
	   <div class="col-lg-10 col-xs-10 float_right"><input class="form_control" type="email" name="mail" style="width: 100%; height: 33px;"> </div>
	   <div class="col-lg-2 col-xs-2">
	  <button class="btn button tasgel">

	  	تسجيل 
	  </button>
	  </div>

	</div>
	</div>
</div>
	
</div>

</div>
<div class="col-lg-12 col-xs-12 col-md-12 second_part">
<div class="container">
<div class="col-lg-12 col-xs-12 col-md-12 Logo_footer_about_us">
<div class="col-lg-2 col-xs-3 col-md-3 float_right">
<img  class="logofooter" src="<?php echo get_template_directory_uri();?>/Images/footerLogo.png">

</div>
<div class="col-lg-7 col-xs-7 col-md-7 text_footer float_right">
<ul class="col-lg-3 col-xs-12 col-md-3 float_right ">
	<li>
	<a href="#"><span> الرئيسيه</span></a>
    </li>
    <li>
	<a href="#"><span> منتجات الشركه</span></a>

	</li>
	<li>
	<a href="#"><span> طلب التمويل</span></a>

	</li>
</ul>
<div class="col-lg-1 col-xs-1"><br></div>
<ul class="col-lg-3 col-xs-12 col-md-3 float_right ">
	<li>
	<a href="#"><span> الاداره</span></a>
    </li>
    <li>
	<a href="#"><span> من نحن</span></a>

	</li>
	<li>
	<a href="#"><span> اتصل بنا </span></a>

	</li>
</ul>
</div>
<div class="col-lg-3 col-xs-3 float_right SocialMedia">
<img src="<?php echo get_template_directory_uri();?>/Images/fb.png">
<img src="<?php echo get_template_directory_uri();?>/Images/Twitter.png">
</div>

</div>

</div>

</div>
<div class="col-lg-12 col-md-12 thirdPart hidden-xs hidden-sm ">
<div class="container">
<div class="col-lg-12 col-xs-12 col-md-12 ">
<img src="<?php echo get_template_directory_uri();?>/Images/copyright.png">

	
</div>
	
</div>

</div>
</footer>

</div><!-- .site -->

<?php wp_footer(); ?>

</body>
</html>
