<?php
/*
 * Template Name: Blog Template
 *
 *
 * */
get_header();?>
<div id="main-content" class="main-wrapper st-default content-page">
    <?php do_action('s7upf_before_main_content')?>
    <div id="tp-blog-page" class="tp-blog-page">
        <?php s7upf_header_image();?>
        <div class="container">
        <?php s7upf_display_breadcrumb();?>
            <div class="row">
                <?php s7upf_output_sidebar('left')?>
                <div class="blog-list <?php echo esc_attr(s7upf_get_main_class()); ?>">
                    <?php 
                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                        $query = new WP_Query('post_type=post&paged='.$paged );
                    ?>
                    <?php if($query -> have_posts()):?>    
                        <?php while ($query -> have_posts()) :$query -> the_post();?>
                            <?php get_template_part('s7upf_templates/blog-content/content')?>
                        <?php endwhile;?>
                        <?php wp_reset_postdata();?>

                        <?php 
                            $big = 999999999;
                            echo paginate_links( array(
                                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                                'format'       => '&page=%#%',
                                'current'      => max( 1, $paged ),
                                'total'        => $query->max_num_pages,                                    
                                'mid_size'     => 1,
                                'type'         => 'list',
                                'prev_text'    => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                                'next_text'    => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                            ) )
                        ?>

                    <?php else : ?>
                        <?php get_template_part( 's7upf_templates/blog-content/content', 'none' ); ?>
                    <?php endif;?>

                </div>
                <?php s7upf_output_sidebar('right')?>
            </div>
        </div>
    </div>
    <?php do_action('s7upf_affter_main_content')?>
</div>
<?php get_footer(); ?>
