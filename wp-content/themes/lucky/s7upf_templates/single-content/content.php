<?php
$data = $st_link_post= $s_class = '';
global $post;
$s7upf_image_blog = get_post_meta(get_the_ID(), 'format_image', true);
if(!empty($s7upf_image_blog)){
    $data .='<div class="single-post-thumb banner-adv fade-in-out">
                <div class="adv-thumb-link">
                    <img alt="'.esc_attr($post->post_name).'" title="'.esc_attr($post->post_name).'" src="' . esc_url($s7upf_image_blog) . '"/>
                </div>
            </div>';
}
else{
    if (has_post_thumbnail()) {
        $data .=    '<div class="single-post-thumb banner-adv fade-in-out adv-thumb-link">
                        <div class="adv-thumb-link">
                            '.get_the_post_thumbnail(get_the_ID(),'full').'
                        </div>
                    </div>';
    }
}
?>
<div class="row">
    <div class="col-md-5 col-sm-6 col-xs-12">
        <h2 class="post-title"><?php the_title()?></h2>
        <div class="post-info">
            <?php s7upf_display_metabox()?>
            <div class="share-social">
                <label><?php esc_html_e("Share","lucky")?>:</label>
                <a href="http://www.facebook.com/sharer.php?u=<?php echo get_the_permalink();?>"><i class="fa fa-facebook-square"></i></a>
                <a href="http://twitthis.com/twit?url=<?php echo get_the_permalink();?>"><i class="fa fa-twitter-square"></i></a>
                <a href="https://plus.google.com/share?url=<?php echo get_the_permalink();?>"><i class="fa fa-google-plus-square"></i></a>
                <a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&amp;media=<?php if(function_exists('the_post_thumbnail')) echo wp_get_attachment_url(get_post_thumbnail_id()); ?>"><i class="fa fa-pinterest-square"></i></a>
            </div>
        </div>
        <div class="main-single-content">
        <?php
            $f_content = get_post_meta(get_the_ID(),'first_intro',true);
            echo balanceTags($f_content);
        ?>
        </div>
    </div>
    <div class="col-md-7 col-sm-6 col-xs-12">
        <?php if(!empty($data)) echo balanceTags($data);?>
    </div>
</div>
<div class="main-single-content">
    <?php the_content()?>
</div>
