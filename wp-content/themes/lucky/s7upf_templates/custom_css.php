<?php
/**
 * Created by Sublime Text 2.
 * User: thanhhiep992
 * Date: 13/08/15
 * Time: 10:20 AM
 */
$main_color = s7upf_get_value_by_id('main_color');
$data_attr = s7upf_get_option('woo_attr_background');
?>
<?php
$style = '';

/*****BEGIN TERM BG COLOR*****/
if(!empty($data_attr)){
    foreach ($data_attr as $attr) {
        $style .= '.bgcolor-'.$attr['attr_slug'].'
        {border: 1px solid #e5e5e5;
        display: block;
        height: 30px;
        margin: 2px 1px;
        padding: 1px !important;
        width: 30px;
        font-size:0}'."\n";
        $style .=   '.termbg-'.$attr['attr_slug'].' span{background-color:'.$attr['attr_bg'].'}'."\n";
        $style .=   '.termbg-'.$attr['attr_slug'].'{font-size:0;}'."\n";
        $style .= '.bgcolor-'.$attr['attr_slug'].' span
        {background-color:'.$attr['attr_bg'].';
        display: block;
        height: 100%;
        overflow: hidden;
        position: relative;
        width: 100%;}'."\n";
    }
}
/*****END TERM BG COLOR*****/

/*****BEGIN MAIN COLOR*****/
if(!empty($main_color)){
	$style .= '.color, a:focus, a:hover,.top-right>ul>li>a.mycart-link .number-cart-total,
    .comment-author a:hover, .comment-reply, .content-single .post-info .share-social a:hover, .content-single .post-info a:hover, .control-post .next-post:hover, .control-post .prev-post:hover, .item-post-gallery h3 a:hover, .item-service2:hover .service-info p, .item-service2:hover .service-info p a, .list-post-tags a:hover, .main-nav>ul>li:hover>a, .post-title a:hover,
    .banner-info.adv-info-style1 .shopnow:hover, .title-tab1 .list-none li.active a, .title-tab1 .list-none li:hover a,
    .btn-rect.dotted:hover,.download-link a:hover,.comment-form .controls a:hover,
    .owl-theme.arrow-style3 .owl-controls .owl-buttons div::after, .top-right.box-meta3>ul>li:hover>a,
    .item-product-masonry .product-title a:hover, .title-box5 .list-none li.active a,
    .deal-slider8 .owl-theme .owl-controls .owl-buttons div:hover, .hotdeal-countdown.clock-countdown .number, .top-right.box-meta8>ul>li:hover>a,
    .product-gallery a.bx-next:hover, .product-gallery a.bx-prev:hover,.deal-palallax .time_circles>div>.number,
    .banner-adv.item-adv11 .banner-info .color, .nav-tabs-icon>ul>li.active>a,
    .item-cat-hover-dir .btn-rect:hover, .title-tab2>ul>li.active>a,
    .owl-theme.arrow-style14 .owl-controls .owl-buttons div:hover, .text-info-special .title60 span,
    .item-hotcat14 .hotcat-info .title18 a:hover,.arrow-style14 .owl-theme .owl-controls .owl-buttons div:hover
    {color:'.$main_color.'}'."\n";
    
    $style .= '.register-box .link-signin:hover,.product-extra-link>a:hover,.product-thumb .quickview-link,
    .btn-product-loadmore:hover,.newsletter-form form input[type=submit]:hover, .post-thumb-zoom:hover, .radius.scroll-top:hover,
    .btn-rect:hover,.owl-theme .owl-controls .owl-buttons div:hover,.footer-menu-box .list-none>li a:hover::before, .footer-nav li a:hover::before,
    .btn-link-default:hover, .newsletter-footer input[type=submit]:hover,.social-footer>a:hover,
    .slider-range-price, .slider-range-price .ui-slider-handle.ui-state-default.ui-corner-all,
    .item-latest-news.style2 .readmore:hover,.banner-info.adv-info-style2 .title30, .slick-nav:hover,
    .woocommerce .widget_price_filter .ui-slider .ui-slider-range,.top-header.top-header5,.bg-color,
    .woocommerce button.button.alt:hover, .woocommerce #respond input#submit:hover, .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover,
    .top-extra-deal li .btn-rect,.banner-slider11 .owl-theme .owl-controls .owl-page.active span,
    .nav-tabs-icon>ul>li.active>a::after,.percent-store, .product-extra-link11>a.addcart-link:hover,.about-testimo-slider,
    .inner-cat-hover-dir,.paginumber-slider .owl-theme .owl-controls .owl-page.active span,
    .item-hotcat14 .hotcat-info .btn-rect:hover,.about-testimonial,.item-service2:hover .service-icon a
    {background-color:'.$main_color.'}'."\n";

    $style .= '.btn-product-loadmore:hover,.btn-rect.dotted:hover,.tagcloud>a:hover,
    .footer-menu-box .list-none>li a:hover::before, .footer-nav li a:hover::before,.social-footer>a:hover,
    .woocommerce .widget_price_filter .ui-slider .ui-slider-handle,.deal-palallax .time_circles>div,
    .deal-control>ul>li>a.active,.box-product-type.shadow-box,.title16.title-box12,
    .item-product-deal12 .product-extra-link>a:hover,.paginumber-slider .owl-theme .owl-controls .owl-page.active span,
    .paginumber-slider .owl-theme .owl-controls .owl-page:hover span,.item-service2:hover .service-icon a
    {border-color: '.$main_color.'}'."\n";
}
/*****END MAIN COLOR*****/

/*****BEGIN CUSTOM CSS*****/
$custom_css = s7upf_get_option('custom_css');
if(!empty($custom_css)){
    $style .= $custom_css."\n";
}

/*****END CUSTOM CSS*****/

/*****BEGIN MENU COLOR*****/
$menu_color = s7upf_get_option('s7upf_menu_color');
$menu_hover = s7upf_get_option('s7upf_menu_color_hover');
$menu_active = s7upf_get_option('s7upf_menu_color_active');
$menu_color2 = s7upf_get_option('s7upf_menu_color2');
$menu_hover2 = s7upf_get_option('s7upf_menu_color_hover2');
$menu_active2 = s7upf_get_option('s7upf_menu_color_active2');
if(is_array($menu_color) && !empty($menu_color)){
    $style .= '.main-nav > ul > li > a,.main-nav.main-nav3>ul>li>a{';
    if(!empty($menu_color['font-color'])) $style .= 'color:'.$menu_color['font-color'].';';
    if(!empty($menu_color['font-family'])) $style .= 'font-family:'.$menu_color['font-family'].';';
    if(!empty($menu_color['font-size'])) $style .= 'font-size:'.$menu_color['font-size'].';';
    if(!empty($menu_color['font-style'])) $style .= 'font-style:'.$menu_color['font-style'].';';
    if(!empty($menu_color['font-variant'])) $style .= 'font-variant:'.$menu_color['font-variant'].';';
    if(!empty($menu_color['font-weight'])) $style .= 'font-weight:'.$menu_color['font-weight'].';';
    if(!empty($menu_color['letter-spacing'])) $style .= 'letter-spacing:'.$menu_color['letter-spacing'].';';
    if(!empty($menu_color['line-height'])) $style .= 'line-height:'.$menu_color['line-height'].';';
    if(!empty($menu_color['text-decoration'])) $style .= 'text-decoration:'.$menu_color['text-decoration'].';';
    if(!empty($menu_color['text-transform'])) $style .= 'text-transform:'.$menu_color['text-transform'].';';
    $style .= '}'."\n";
}
if(!empty($menu_hover)){
    $style .= '.main-nav > ul > li:hover > a:focus,.main-nav > ul > li:hover > a,
    .main-nav > ul li.current-menu-ancestor > a,.main-nav > ul li.current-menu-item > a,
    .main-nav.main-nav3 > ul > li:hover > a:focus,.main-nav.main-nav3 > ul > li:hover > a,
    .main-nav.main-nav3 > ul li.current-menu-ancestor > a,.main-nav.main-nav3> ul li.current-menu-item > a
    {color:'.$menu_hover.'}'."\n";
}
if(!empty($menu_active)){
    $style .= '.main-nav > ul > li:hover, .main-nav > ul >li.current-menu-ancestor,
    .main-nav > ul > li.current-menu-item,.main-nav.main-nav3 > ul > li:hover, .main-nav.main-nav3 > ul >li.current-menu-ancestor,
    .main-nav.main-nav3 > ul > li.current-menu-item
    {background-color:'.$menu_active.'}'."\n";
}

// Sub menu
if(is_array($menu_color2) && !empty($menu_color2)){
    $style .= '.main-nav > ul > li.menu-item-has-children li > a{';
    if(!empty($menu_color2['font-color'])) $style .= 'color:'.$menu_color2['font-color'].';';
    if(!empty($menu_color2['font-family'])) $style .= 'font-family:'.$menu_color2['font-family'].';';
    if(!empty($menu_color2['font-size'])) $style .= 'font-size:'.$menu_color2['font-size'].';';
    if(!empty($menu_color2['font-style'])) $style .= 'font-style:'.$menu_color2['font-style'].';';
    if(!empty($menu_color2['font-variant'])) $style .= 'font-variant:'.$menu_color2['font-variant'].';';
    if(!empty($menu_color2['font-weight'])) $style .= 'font-weight:'.$menu_color2['font-weight'].';';
    if(!empty($menu_color2['letter-spacing'])) $style .= 'letter-spacing:'.$menu_color2['letter-spacing'].';';
    if(!empty($menu_color2['line-height'])) $style .= 'line-height:'.$menu_color2['line-height'].';';
    if(!empty($menu_color2['text-decoration'])) $style .= 'text-decoration:'.$menu_color2['text-decoration'].';';
    if(!empty($menu_color2['text-transform'])) $style .= 'text-transform:'.$menu_color2['text-transform'].';';
    $style .= '}'."\n";
}
if(!empty($menu_hover2)){
    $style .= '.main-nav > ul > li.menu-item-has-children li.menu-item-has-children > a:focus,
    .main-nav > ul > li.menu-item-has-children li.menu-item-has-children:hover > a,
    .main-nav > ul .sub-menu > li.current-menu-item > a,
    .main-nav > ul > li .sub-menu > li:hover>a,
    .main-nav > ul > li.menu-item-has-children li.current-menu-item> a,
    .main-nav > ul > li.menu-item-has-children li.current-menu-ancestor > a
    {color:'.$menu_hover2.'}'."\n";
}
if(!empty($menu_active2)){
    $style .= '.main-nav > ul > li.menu-item-has-children li.menu-item-has-children:hover,
    .main-nav > ul > li.menu-item-has-children li.current-menu-ancestor,
    .main-nav > ul > li.menu-item-has-children li.current-menu-item,
    .main-nav>ul>li:not(.has-mega-menu) .sub-menu> li:hover,
    .main-nav > ul > li.menu-item-has-children li.current-menu-ancestor
    {background-color:'.$menu_active2.'}'."\n";
}

/*****END MENU COLOR*****/

/*****BEGIN TYPOGRAPHY*****/
$typo_data = s7upf_get_option('s7upf_custom_typography');
if(is_array($typo_data) && !empty($typo_data)){
    foreach ($typo_data as $value) {
        switch ($value['typo_area']) {
            case 'header':
                $style_class = '.site-header';
                break;

            case 'footer':
                $style_class = '#footer';
                break;

            case 'widget':
                $style_class = '.widget';
                break;
            
            default:
                $style_class = '#main-content';
                break;
        }
        $class_array = explode(',', $style_class);
        $new_class = '';
        if(is_array($class_array)){
            foreach ($class_array as $prefix) {
                $new_class .= $prefix.' '.$value['typo_heading'].',';
            }
        }
        if(!empty($new_class)) $style .= $new_class.' .nocss{';
        if(!empty($value['typography_style']['font-color'])) $style .= 'color:'.$value['typography_style']['font-color'].';';
        if(!empty($value['typography_style']['font-family'])) $style .= 'font-family:'.$value['typography_style']['font-family'].';';
        if(!empty($value['typography_style']['font-size'])) $style .= 'font-size:'.$value['typography_style']['font-size'].';';
        if(!empty($value['typography_style']['font-style'])) $style .= 'font-style:'.$value['typography_style']['font-style'].';';
        if(!empty($value['typography_style']['font-variant'])) $style .= 'font-variant:'.$value['typography_style']['font-variant'].';';
        if(!empty($value['typography_style']['font-weight'])) $style .= 'font-weight:'.$value['typography_style']['font-weight'].';';
        if(!empty($value['typography_style']['letter-spacing'])) $style .= 'letter-spacing:'.$value['typography_style']['letter-spacing'].';';
        if(!empty($value['typography_style']['line-height'])) $style .= 'line-height:'.$value['typography_style']['line-height'].';';
        if(!empty($value['typography_style']['text-decoration'])) $style .= 'text-decoration:'.$value['typography_style']['text-decoration'].';';
        if(!empty($value['typography_style']['text-transform'])) $style .= 'text-transform:'.$value['typography_style']['text-transform'].';';
        $style .= '}';
        $style .= "\n";
    }
}
/*****END TYPOGRAPHY*****/
if(!empty($style)) print $style;
?>