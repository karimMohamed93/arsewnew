<?php
$data = $icon = '';
if (has_post_thumbnail()) {
    $data .=    '<div class="col-md-5 col-sm-5 col-xs-5">
                    <div class="post-thumb">
                        <a href="'. esc_url(get_the_permalink()) .'" class="post-thumb-link">'.get_the_post_thumbnail(get_the_ID(),array(360,240)).'</a>                
                        <a href="'.get_the_post_thumbnail_url(get_the_ID(),'full').'" class="post-thumb-zoom"><i class="fa fa-search" aria-hidden="true"></i></a>
                    </div>
                </div>';
    $main_col = 'col-md-7 col-sm-7 col-xs-7';
}
else{
    $main_col = 'col-md-12 col-sm-12 col-xs-12';
}
?>
<div class="item-blog-list">
    <div class="row">
        <?php if(!empty($data)) echo balanceTags($data);?>
        <div class="<?php echo esc_attr($main_col)?>">
            <div class="post-info">
                <h3 class="post-title"><a href="<?php echo esc_url(get_the_permalink());?>">
                    <?php the_title()?>
                    <?php echo (is_sticky()) ? '<i class="fa fa-thumb-tack"></i>':''?>
                </a></h3>
                <?php s7upf_display_metabox()?>
                <p class="desc"><?php echo get_the_excerpt(); ?></p>
                <a href="<?php the_permalink();?>" class="btn-link-default radius"><?php echo esc_html_e("Read more","lucky")?></a>
            </div>
        </div>
    </div>
</div>