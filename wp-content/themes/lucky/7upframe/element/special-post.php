<?php
/**
 * Created by Sublime text 2.
 * User: thanhhiep992
 * Date: 23/12/15
 * Time: 10:00 AM
 */

if(!function_exists('s7upf_vc_post_special'))
{
    function s7upf_vc_post_special($attr)
    {
        $html = $data = '';
        extract(shortcode_atts(array(
            'style'     => '',
            'post'      => '',
            'id_post'   => '',
            'size'      => '',
            'thumbnail' => '',
        ),$attr));
        if(empty($id_post)) $id_post = $post;
        if(!empty($id_post)){
            $query = new WP_Query( array(
                'post_type' => 'post',
                'post__in' => array($id_post)
                ));
            if( $query->have_posts() ):
                while ( $query->have_posts() ) : $query->the_post();
                    switch ($style) {
                        case 'home-2':
                            
                            break;
                        
                        default:                        
                            if(!empty($size)) $size = explode('x', $size);
                            else $size = array(570,410);
                            $thumbnail_html = get_the_post_thumbnail(get_the_ID(),$size);
                            if(!empty($thumbnail)) $thumbnail_html = wp_get_attachment_image($thumbnail,'full');
                            $format = get_post_format();
                            switch ($format) {
                                case 'video':
                                    if (get_post_meta(get_the_ID(), 'format_media', true)) {
                                        $media_url = get_post_meta(get_the_ID(), 'format_media', true);
                                        $data .='<div class="post-video">';
                                        $data .= s7upf_remove_w3c(wp_oembed_get($media_url));
                                        $data .='</div>';
                                    }
                                    else $data = $thumbnail_html;
                                    break;

                                case 'gallery':
                                    $gallery = get_post_meta(get_the_ID(), 'format_gallery', true);
                                    if (!empty($gallery)){
                                        $array = explode(',', $gallery);
                                        if(is_array($array) && !empty($array)){
                                            $data .= '<div class="wrap-item smart-slider arrow-style3" data-item="1" data-speed="" data-itemres="" data-prev="fa-arrow-circle-left" data-next="fa-arrow-circle-right" data-pagination="" data-navigation="true">';
                                            foreach ($array as $key => $item) {
                                                $data .=    '<div class="post-leading3">
                                                                <a href="'.esc_url(get_the_permalink()).'">
                                                                    '.wp_get_attachment_image($item,$size).'
                                                                </a>
                                                            </div>';
                                            }        
                                            $data .='</div>';
                                        }
                                    }
                                    else $data = $thumbnail_html;
                                    break;

                                case 'audio':
                                    if (get_post_meta(get_the_ID(), 'format_media', true)) {
                                        $media_url = get_post_meta(get_the_ID(), 'format_media', true);
                                        $data .= '<div class="audio">' . s7upf_remove_w3c(wp_oembed_get($media_url, array('height' => '176'))) . '</div>';
                                    }
                                    else $data = $thumbnail_html;
                                    break;
                                
                                default:
                                    $data = $thumbnail_html;
                                    break;
                            }
                            $html .=    '<div class="item-latest-post3 latest-post-leading3">
                                            '.$data.'
                                            <div class="latest-post-info3">
                                                <h3 class="post-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                                <ul class="post-comment-date list-none">
                                                    <li><i class="fa fa-calendar-minus-o" aria-hidden="true"></i><span>'.get_the_date('M d Y').'</span></li>
                                                    <li><i class="fa fa-comment-o" aria-hidden="true"></i><span>'.get_comments_number().' '.esc_html__("Comments","lucky").'</span></li>
                                                </ul>
                                                <p class="desc">'.s7upf_substr(get_the_excerpt(),0,100).'</p>
                                            </div>
                                        </div>';
                            break;
                    }                    
                endwhile;
            endif;
            wp_reset_postdata();            
        }
        return $html;
    }
}

stp_reg_shortcode('sv_post_special','s7upf_vc_post_special');

vc_map( array(
    "name"      => esc_html__("SV Post", 'lucky'),
    "base"      => "sv_post_special",
    "icon"      => "icon-st",
    "category"  => '7Up-theme',
    "params"    => array(
        array(
            "type" => "dropdown",
            "holder" => "div",
            "heading" => esc_html__("Style",'lucky'),
            "param_name" => "style",
            "value"     => array(
                esc_html__( 'Default', 'lucky' )    => '',
                ),
            'description' => esc_html__( 'Select post to show.', 'lucky' ),
        ),
        array(
            "type" => "dropdown",
            "holder" => "div",
            "heading" => esc_html__("Post",'lucky'),
            "param_name" => "post",
            "value"     => s7upf_get_list_post(),
            'description' => esc_html__( 'Select post to show.', 'lucky' ),
        ),
        array(
            "type" => "textfield",
            "holder" => "div",
            "heading" => esc_html__("Post ID",'lucky'),
            "param_name" => "id_post",
            'description' => esc_html__( 'Enter post ID to show. Default is select post value.', 'lucky' ),
        ),        
        array(
            "type"          => "textfield",
            "heading"       => esc_html__("Size Thumbnail",'lucky'),
            "param_name"    => "size",
            'description' => esc_html__( 'Enter site thumbnail to crop. [width]x[height]. Example is 300x300', 'lucky' ),
        ),
        array(
            "type" => "attach_image",
            "holder" => "div",
            "heading" => esc_html__("Custom Thumbnail",'lucky'),
            "param_name" => "thumbnail",
        )
    )
));