<?php
/**
 * Created by Sublime text 2.
 * User: thanhhiep992
 * Date: 12/08/15
 * Time: 10:00 AM
 */

if(!function_exists('s7upf_vc_map'))
{
    function s7upf_vc_map($attr)
    {
        $html = '';
        extract(shortcode_atts(array(
            'style'         =>'',
            'market'        =>'',
            'zoom'          =>'16',
            'location'      =>'',
            'control'       =>'yes',
            'scrollwheel'   =>'yes',
            'disable_ui'    =>'no',
            'draggable'     =>'yes',
            'width'     =>'100%',
            'height'     =>'500px'
        ),$attr));
        parse_str( urldecode( $location ), $locations);
        $location_text = '';
        foreach ($locations as $values) {
            $location_text .= '|';
            foreach ($values as $value) {
                $location_text .= $value.',';
            }
        }
        $img = array();$img[0]='';
        if($market != '') {
            $img = wp_get_attachment_image_src($market,"full");
        }
        $id = 'sv-map-'.uniqid();
        $map_css = 'width:'.$width.';height:'.$height.';max-width-100%;';
        $html .= '<div class="clearfix"></div><div id="'.esc_attr($id).'" class="sv-ggmaps '.S7upf_Assets::build_css($map_css).'" data-location="'.$location_text.'" data-market="'.$img[0].'" data-zoom="'.$zoom.'" data-style="'.$style.'" data-control="'.$control.'" data-scrollwheel="'.$scrollwheel.'" data-disable_ui="'.$disable_ui.'" data-draggable="'.$draggable.'"></div>';
        return $html;
    }
}

stp_reg_shortcode('sv_map','s7upf_vc_map');

vc_map( array(
    "name"      => esc_html__("SV GoogleMap", 'lucky'),
    "base"      => "sv_map",
    "icon"      => "icon-st",
    "category"  => '7Up-theme',
    "params"    => array(
        array(
            "type" => "dropdown",
            "holder" => "div",
            "heading" => esc_html__("Map Style",'lucky'),
            "param_name" => "style",
            'value' => array(
                esc_html__('Default','lucky') => '',
                esc_html__('Grayscale','lucky') => 'grayscale',
                esc_html__('Blue','lucky') => 'blue',
                esc_html__('Dark','lucky') => 'dark',
                esc_html__('Pink','lucky') => 'pink',
                esc_html__('Light','lucky') => 'light',
                esc_html__('Blueessence','lucky') => 'blueessence',
                esc_html__('Bentley','lucky') => 'bentley',
                esc_html__('Retro','lucky') => 'retro',
                esc_html__('Cobalt','lucky') => 'cobalt',
                esc_html__('Brownie','lucky') => 'brownie'
            ),
        ),
        array(
            "type" => "add_location_map",
            "heading" => esc_html__( "Add Map Location", 'lucky' ),
            "param_name" => "location",
            "description" => esc_html__( "Click Add more button to add location.", 'lucky' )
        ),
        array(
            "type" => "textfield",
            "holder" => "div",
            "heading" => esc_html__( "Map Zoom", 'lucky' ),
            "param_name" => "zoom",
            "description" => esc_html__( "Enter zoom for map. Default is 16", 'lucky' ),
        ),
        array(
            'type' => 'attach_image',
            "holder" => "div",
            'heading' => esc_html__( 'Marker Image', 'lucky' ),
            'param_name' => 'market',
        ),
        array(
            'type' => 'textfield',
            'heading' => esc_html__( 'Map Width', 'lucky' ),
            'param_name' => 'width',
            "description" => esc_html__( "This is value to set width for map. Unit % or px. Example: 100%,500px. Default is 100%", 'lucky' )
        ),
        array(
            'type' => 'textfield',
            'heading' => esc_html__( 'Map Height', 'lucky' ),
            'param_name' => 'height',
            "description" => esc_html__( "This is value to set height for map. Unit % or px. Example: 100%,500px. Default is 500px", 'lucky' )
        ),
        array(
            "type" => "dropdown",
            "heading" => esc_html__("MapTypeControl",'lucky'),
            "param_name" => "control",
            'value' => array(
                esc_html__('Yes','lucky') => 'yes',
                esc_html__('No','lucky') => 'no',
                ),
            'edit_field_class'=>'vc_col-sm-6 vc_column'
        ),
        array(
            "type" => "dropdown",
            "heading" => esc_html__("Scrollwheel",'lucky'),
            "param_name" => "scrollwheel",
            'value' => array(
                esc_html__('Yes','lucky') => 'yes',
                esc_html__('No','lucky') => 'no',
                ),
            'edit_field_class'=>'vc_col-sm-6 vc_column'
        ),
        array(
            "type" => "dropdown",
            "heading" => esc_html__("DisableDefaultUI",'lucky'),
            "param_name" => "disable_ui",
            'value' => array(
                esc_html__('No','lucky') => 'no',
                esc_html__('Yes','lucky') => 'yes',
                ),
            'edit_field_class'=>'vc_col-sm-6 vc_column'
        ),
        array(
            "type" => "dropdown",
            "heading" => esc_html__("Draggable",'lucky'),
            "param_name" => "draggable",
            'value' => array(
                esc_html__('Yes','lucky') => 'yes',
                esc_html__('No','lucky') => 'no',
                ),
            'edit_field_class'=>'vc_col-sm-6 vc_column'
        )
    )
));