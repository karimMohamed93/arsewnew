<?php
/**
 * Created by Sublime text 2.
 * User: thanhhiep992
 * Date: 12/08/15
 * Time: 10:00 AM
 */

if(!function_exists('s7upf_vc_shop'))
{
    function s7upf_vc_shop($attr)
    {
        $html = '';
        extract(shortcode_atts(array(
            'style'      => 'grid',
            'number'     => '12',
            'orderby'    => 'menu_order',
            'column'     => '1',
        ),$attr));
        $type = $style;
        if(isset($_GET['orderby'])){
            $orderby = $_GET['orderby'];
        }
        if(isset($_GET['type'])){
            $type = $_GET['type'];
        }
        if(isset($_GET['number'])){
            $number = $_GET['number'];
        }
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args = array(
            'post_type'         => 'product',
            'posts_per_page'    => $number,
            'paged'             => $paged,
            );
        $attr_taxquery = array();
        global $wpdb;
        $attribute_taxonomies = wc_get_attribute_taxonomies();
        if(!empty($attribute_taxonomies)){
            foreach($attribute_taxonomies as $attr){
                if(isset($_REQUEST['pa_'.$attr->attribute_name])){
                    $term = $_REQUEST['pa_'.$attr->attribute_name];
                    $term = explode(',', $term);
                    $attr_taxquery[] =  array(
                                            'taxonomy'      => 'pa_'.$attr->attribute_name,
                                            'terms'         => $term,
                                            'field'         => 'slug',
                                            'operator'      => 'IN'
                                        );
                }
            }            
        }
        if(isset( $_GET['product_cat'])) $cats = $_GET['product_cat'];
        if(!empty($cats)) {
            $cats = explode(",",$cats);
            $attr_taxquery[]=array(
                'taxonomy'=>'product_cat',
                'field'=>'slug',
                'terms'=> $cats
            );
        }
        if ( !is_admin() && !empty($attr_taxquery)){
            $attr_taxquery = array('relation ' => 'AND');
            $args['meta_query'][]  = array(
                'key'           => '_visibility',
                'value'         => array('catalog', 'visible'),
                'compare'       => 'IN'
            );
            $args['tax_query'] = $attr_taxquery;
        }
        if( isset( $_GET['min_price']) && isset( $_GET['max_price']) ){
            $min = $_GET['min_price'];
            $max = $_GET['max_price'];
            $args['post__in'] = s7upf_filter_price($min,$max);
        }
        switch ($orderby) {
            case 'price' :
                $args['orderby']  = "meta_value_num ID";
                $args['order']    = 'ASC';
                $args['meta_key'] = '_price';
            break;

            case 'price-desc' :
                $args['orderby']  = "meta_value_num ID";
                $args['order']    = 'DESC';
                $args['meta_key'] = '_price';
            break;

            case 'popularity' :
                $args['meta_key'] = 'total_sales';
                add_filter( 'posts_clauses', array( WC()->query, 'order_by_popularity_post_clauses' ) );
            break;

            case 'rating' :
                $args['meta_key'] = '_wc_average_rating';
                $args['orderby'] = 'meta_value_num';
                $args['meta_query'] = WC()->query->get_meta_query();
                $args['tax_query'][] = WC()->query->get_tax_query();
            break;

            case 'date':
                $args['orderby'] = 'date';
                break;
            
            default:
                $args['orderby'] = 'menu_order title';
                break;
        }
        $grid_active = $list_active = '';
        if($type == 'grid') $grid_active = 'active'; 
        if($type == 'list') $list_active = 'active';
        $product_query = new WP_Query($args);
        $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
        ob_start();?>
        <div class="main-shop-load woocommerce">
        <div class="sort-pagi-bar top clearfix">
            <ul class="product-sort pull-left list-inline">
                <li><a data-type="grid" href="<?php echo esc_url(s7upf_get_key_url('type','grid'))?>" class="load-shop-ajax grid <?php if($type == 'grid') echo 'active'?>"><?php esc_html_e("Grid","lucky")?></a></li>
                <li><a data-type="list" href="<?php echo esc_url(s7upf_get_key_url('type','list'))?>" class="load-shop-ajax list <?php if($type == 'list') echo 'active'?>"><?php esc_html_e("List","lucky")?></a></li>
            </ul>
            <?php if($product_query->max_num_pages > 1){?>
            <div class="product-pagi-nav pull-right list-inline">
                <?php
                    echo paginate_links( array(
                        'base'         => esc_url_raw( str_replace( 999999999, '%#%', remove_query_arg( 'add-to-cart', get_pagenum_link( 999999999, false ) ) ) ),
                        'format'       => '',
                        'add_args'     => '',
                        'current'      => max( 1, get_query_var( 'paged' ) ),
                        'total'        => $product_query->max_num_pages,
                        'prev_text'    => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                        'next_text'    => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                        'type'         => 'list',
                        'end_size'     => 2,
                        'mid_size'     => 1
                    ) );
                ?>
            </div>
            <?php }?>
            <div class="product-filter pull-right">
                <?php s7upf_catalog_ordering($product_query,$orderby)?>
            </div>
        </div>
        <div class="product-<?php echo esc_attr($type)?>">
            <ul class="list-product row list-unstyled" data-number="<?php echo esc_attr($number)?>" data-column="<?php echo esc_attr($column)?>" data-currency="<?php echo esc_attr(get_woocommerce_currency_symbol())?>">
        <?php
        $count_product = 1;
        if($product_query->have_posts()) {
            while($product_query->have_posts()) {
                $product_query->the_post();
                global $product;
                ?>
                <?php if($type == 'list'){
                    $list_text = get_the_excerpt();
                    ?>
                    <li class="col-md-12 col-sm-12 col-xs-12">  
                    <?php
                        echo '<div class="item-product clearfix">
                                    <div class="product-thumb">
                                        <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),array(250,310)).'</a>
                                        '.s7upf_product_link().'
                                        <a data-product-id="'.get_the_id().'" href="'.esc_url(get_the_permalink()).'" class="product-quick-view quickview-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="product-info">
                                        <h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                        '.s7upf_get_rating_html().'
                                        <p class="desc">'.$list_text.'</p>
                                        <div class="wrap-cart-qty">
                                            '.s7upf_get_price_html().'
                                            <div class="info-extra product">
                                                '.s7upf_wishlist_link().'
                                                '.s7upf_compare_url().'
                                            </div>';
                                            woocommerce_template_single_add_to_cart();
                        echo            '</div>
                                    </div>
                                </div>';
                    ?>
                </li>
                <?php }
                else{
                    $col_option = $column;
                    switch ($col_option) {
                        case 1:
                            $size = 'full';
                            $col = 'col-md-12 col-sm-12';
                            break;

                        case 2:
                            $size = array(250*1.5,310*1.5);
                            $col = 'col-md-6 col-sm-6';
                            break;
                        
                        case 3:
                            $size = array(250*1.2,310*1.2);
                            $col = 'col-md-4 col-sm-6';
                            break;

                        case 6:
                            $col = 'col-md-2 col-sm-3';
                            $size = array(250,310);
                            break;

                        default:
                            $col = 'col-md-3 col-sm-4';
                            $size = array(250,310);
                            break;
                    }
                ?>
                <li class="<?php echo esc_attr($col)?> col-xs-6">
                    <?php
                        echo '<div class="item-product">
                                <div class="product-thumb">
                                    <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),$size).'</a>
                                    '.s7upf_product_link().'
                                    <a data-product-id="'.get_the_id().'" href="'.esc_url(get_the_permalink()).'" class="product-quick-view quickview-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                                    '.s7upf_get_rating_html().'
                                    '.s7upf_get_saleoff_html().'
                                </div>
                                <div class="product-info">
                                    <h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                    '.s7upf_get_price_html().'
                                </div>
                            </div>';
                    ?>
                </li>
                <?php 
                $count_product++;
                }
            }
        }
        ?>
            </ul>
        </div>
        <div class="sort-pagi-bar top clearfix">
            <ul class="product-sort pull-left list-inline">
                <li><a data-type="grid" href="<?php echo esc_url(s7upf_get_key_url('type','grid'))?>" class="load-shop-ajax grid <?php if($type == 'grid') echo 'active'?>"><?php esc_html_e("Grid","lucky")?></a></li>
                <li><a data-type="list" href="<?php echo esc_url(s7upf_get_key_url('type','list'))?>" class="load-shop-ajax list <?php if($type == 'list') echo 'active'?>"><?php esc_html_e("List","lucky")?></a></li>
            </ul>
            <?php if($product_query->max_num_pages > 1){?>
            <div class="product-pagi-nav pull-right list-inline">
                <?php
                    echo paginate_links( array(
                        'base'         => esc_url_raw( str_replace( 999999999, '%#%', remove_query_arg( 'add-to-cart', get_pagenum_link( 999999999, false ) ) ) ),
                        'format'       => '',
                        'add_args'     => '',
                        'current'      => max( 1, get_query_var( 'paged' ) ),
                        'total'        => $product_query->max_num_pages,
                        'prev_text'    => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                        'next_text'    => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                        'type'         => 'list',
                        'end_size'     => 2,
                        'mid_size'     => 1
                    ) );
                ?>
            </div>
            <?php }?>
            <div class="product-filter pull-right">
                <?php s7upf_catalog_ordering($product_query,$orderby)?>
            </div>
        </div>
        </div>
        <?php
        $html .= ob_get_clean();
        wp_reset_postdata();
        return $html;
    }
}

stp_reg_shortcode('sv_shop','s7upf_vc_shop');

vc_map( array(
    "name"      => esc_html__("SV Shop", 'lucky'),
    "base"      => "sv_shop",
    "icon"      => "icon-st",
    "category"  => '7Up-theme',
    "params"    => array(
        array(
            "type" => "dropdown",
            "heading" => esc_html__("Style",'lucky'),
            "param_name" => "style",
            "value"     => array(
                esc_html__("Grid",'lucky')   => 'grid',
                esc_html__("List",'lucky')   => 'list',
                ),
            ),
        array(
            'heading'     => esc_html__( 'Number', 'lucky' ),
            'type'        => 'textfield',
            'description' => esc_html__( 'Enter number of product. Default is 12.', 'lucky' ),
            'param_name'  => 'number',
            ),
        array(
            "type" => "dropdown",
            "heading" => esc_html__("Order By",'lucky'),
            "param_name" => "orderby",
            "value"         => array(
                esc_html__( 'Default sorting (custom ordering + name)', 'lucky' ) => 'menu_order',
                esc_html__( 'Popularity (sales)', 'lucky' )       => 'popularity',
                esc_html__( 'Average Rating', 'lucky' )           => 'rating',
                esc_html__( 'Sort by most recent', 'lucky' )      =>'date',
                esc_html__( 'Sort by price (asc)', 'lucky' )      => 'price',
                esc_html__( 'Sort by price (desc)', 'lucky' )     =>'price-desc',
                ),
            ),
        array(
            "type" => "dropdown",
            "heading" => esc_html__("Column",'lucky'),
            "param_name" => "column",
            "value"         => array(
                esc_html__("1 Column","lucky")          => '1',
                esc_html__("2 Column","lucky")          => '2',
                esc_html__("3 Column","lucky")          => '3',
                esc_html__("4 Column","lucky")          => '4',
                esc_html__("6 Column","lucky")          => '6',
                ),
            "dependency"    => array(
                "element"   => "style",
                "value"   => "grid",
                )
            ),
        ),
));