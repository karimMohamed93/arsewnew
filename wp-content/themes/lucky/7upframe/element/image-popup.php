<?php
/**
 * Created by Sublime text 2.
 * User: thanhhiep992
 * Date: 12/08/15
 * Time: 10:00 AM
 */

if(!function_exists('s7upf_vc_image_popup'))
{
    function s7upf_vc_image_popup($attr)
    {
        $html = '';
        extract(shortcode_atts(array(
            'style'      => 'popup',
            'image'      => '',
            'title'      => '',
            'link'      => '',
        ),$attr));
        if(!empty($image)){
            switch ($style) {
                case 'special':
                    $html .=    '<div class="post-banner-image">
                                    <div class="inner-post-banner-image">
                                        <a href="'.esc_url($link).'" class="post-banner-link">'.wp_get_attachment_image($image,'full').'</a>
                                        <p class="post-banner-text">'.$title.'</p>
                                    </div>
                                </div>';
                    break;
                
                default:
                    $html .=    '<div class="item-post-gallery">
                                    <div class="post-thumb">
                                        <a title="'.esc_attr($title).'" href="'.esc_url($link).'" class="post-thumb-link">'.wp_get_attachment_image($image,'full').'</a>
                                        <a href="'.wp_get_attachment_url($image,'full').'" class="post-thumb-zoom"><i class="fa fa-search" aria-hidden="true"></i></a>
                                    </div>';        
                    if(!empty($title)) $html .=  '<h3><a href="'.esc_url($link).'">'.$title.'</a></h3>';
                    $html .=    '</div>';
                    break;
            }
        }
        return $html;
    }
}

stp_reg_shortcode('sv_image_popup','s7upf_vc_image_popup');

vc_map( array(
    "name"      => esc_html__("SV Image Style", 'lucky'),
    "base"      => "sv_image_popup",
    "icon"      => "icon-st",
    "category"  => '7Up-theme',
    "params"    => array(
        array(
            "type" => "dropdown",
            "heading" => esc_html__("Style",'lucky'),
            "param_name" => "style",
            "value" => array(
                esc_html__("Popup",'lucky')  => 'popup',
                esc_html__("Special",'lucky')  => 'special',
                )
        ),
        array(
            "type" => "attach_image",
            "heading" => esc_html__("Image",'lucky'),
            "param_name" => "image",
        ),
        array(
            "type" => "textfield",
            "holder" => "div",
            "heading" => esc_html__("Title",'lucky'),
            "param_name" => "title",
        ),
        array(
            "type" => "textfield",
            "heading" => esc_html__("Link url",'lucky'),
            "param_name" => "link",
        )
    )
));