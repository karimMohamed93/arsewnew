<?php
/**
 * Created by Sublime text 2.
 * User: thanhhiep992
 * Date: 12/08/15
 * Time: 10:00 AM
 */

if(!function_exists('s7upf_vc_advantage'))
{
    function s7upf_vc_advantage($attr,$content = false)
    {
        $html = $view_html = $link_item = '';
        extract(shortcode_atts(array(
            'style'      => '',
            'image'      => '',
            'link'       => '',
            'color'      => '',
            'time'       => '',
            'info_pos'   => 'text-left',
            'pos_info'   => '',
            'fade'       => '',
            'info_color'       => '',
        ),$attr));
        if(!empty($color)) $color = S7upf_Assets::build_css('background-color:'.$color);
        switch ($style) {
            case 'adv-home1':
                $html .=    '<div class="banner-adv zoom-image '.esc_attr($color).' '.esc_attr($fade).'">
                                <a class="adv-thumb-link" href="'.esc_url($link).'">'.wp_get_attachment_image($image,'full').'</a>
                                <div class="banner-info white">
                                   '.wpb_js_remove_wpautop($content, true).'
                                </div>
                            </div>';
                break;

            case 'adv-countdown14':
                $html .=    '<div class="banner-adv zoom-rotate deal-adv">
                                <a href="'.esc_url($link).'" class="adv-thumb-link">'.wp_get_attachment_image($image,'full').'</a>
                                <div class="banner-info '.esc_attr($pos_info .' '.$info_color.' '.$info_pos).' adv-info-style1">
                                    <div class="hotdeal-countdown" data-date="'.esc_attr($time).'"></div>
                                    '.wpb_js_remove_wpautop($content, true).'
                                    <a href="'.esc_url($link).'" class="shopnow text-uppercase">'.esc_html__("Shop now","lucky").'</a>
                                </div>
                            </div>';
                break;

            case 'adv-home13':
                $html .=    '<div class="banner-adv13">
                                <div class="banner-adv zoom-image blur-image '.esc_attr($fade).'">
                                    <a href="'.esc_url($link).'" class="adv-thumb-link">'.wp_get_attachment_image($image,'full').'</a>
                                    <div class="banner-info text-uppercase adv-info-text-shadow '.esc_attr($pos_info .' '.$info_color.' '.$info_pos.' '.$color).'">
                                        '.wpb_js_remove_wpautop($content, true).'
                                        <a href="'.esc_url($link).'" class="shopnow">'.esc_html__("Shop now","lucky").'</a>
                                    </div>
                                </div>
                            </div>';
                break;

            case 'adv-home12':
                $html .=    '<div class="banner-adv12">
                                <div class="banner-adv zoom-image '.esc_attr($fade).'">
                                    <a href="'.esc_url($link).'" class="adv-thumb-link">'.wp_get_attachment_image($image,'full').'</a>
                                    <div class="banner-info white text-uppercase">
                                        '.wpb_js_remove_wpautop($content, true).'
                                    </div>
                                </div>
                            </div>';
                break;

            case 'adv-home11':
                $html .=    '<div class="banner-adv zoom-image item-adv11 '.esc_attr($fade).'">
                                <a href="'.esc_url($link).'" class="adv-thumb-link">'.wp_get_attachment_image($image,'full').'</a>
                                <div class="banner-info text-uppercase adv-info-style1 '.esc_attr($pos_info .' '.$info_color.' '.$info_pos.' '.$color).' adv-info-bg">
                                    '.wpb_js_remove_wpautop($content, true).'
                                    <a href="'.esc_url($link).'" class="shopnow text-uppercase">'.esc_html__("Shop now","lucky").'</a>
                                </div>
                            </div>';
                break;

            case 'adv-home11-top':
                if(!empty($image)) $image = S7upf_Assets::build_css('background-image: url('.wp_get_attachment_url($image,'full').');');
                $html .=    '<div class="top-banner-prl '.esc_attr($image).'">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <ul class="list-none top-extra-deal">
                                                <li>
                                                    <div class="text-extra-deal '.esc_attr($info_color.' '.$info_pos).' text-uppercase">
                                                        '.wpb_js_remove_wpautop($content, true).'
                                                    </div>
                                                </li>
                                                <li>
                                                    <a href="'.esc_url($link).'" class="btn-rect text-uppercase bg-color">'.esc_html__("Shop now","lucky").'</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">';
                if(!empty($time)) $html .=    '<div class="deal-palallax" data-date="'.esc_attr($time).'"></div>';
                $html .=                '</div>
                                    </div>
                                    <a href="#" class="close-top-deal"><i class="fa fa-times" aria-hidden="true"></i></a>
                                </div>
                            </div>';
                break;

            case 'adv-home10-2':
                $html .=    '<div class="banner-adv zoom-image '.esc_attr($fade).'">
                                <a href="'.esc_url($link).'" class="adv-thumb-link">'.wp_get_attachment_image($image,'full').'</a>
                                <div class="banner-info adv-info-style1 '.esc_attr($pos_info .' '.$info_color.' '.$info_pos).'">';
                if(!empty($content)) $html .=   wpb_js_remove_wpautop($content, true);
                if(!empty($link)) $html .=      '<a href="'.esc_url($link).'" class="shopnow text-uppercase">'.esc_html__("Shop now","lucky").'</a>';
                $html .=        '</div>
                            </div>';
                break;

            case 'adv-home10':
                $html .=    '<div class="banner-adv zoom-rotate '.esc_attr($fade).'">
                                <a href="'.esc_url($link).'" class="adv-thumb-link">'.wp_get_attachment_image($image,'full').'</a>
                                <div class="banner-info adv-info-style1 '.esc_attr($pos_info .' '.$info_color).'">';
                if(!empty($content)) $html .=   wpb_js_remove_wpautop($content, true);
                if(!empty($link)) $html .=      '<a href="'.esc_url($link).'" class="shopnow text-uppercase">'.esc_html__("Shop now","lucky").'</a>';
                $html .=        '</div>
                            </div>';
                break;

            case 'about-info':
                $html .=    '<div class="welcome-banner">
                                <div class="intro-about">
                                    '.wpb_js_remove_wpautop($content, true).'
                                </div>
                                <div class="banner-adv zoom-image '.esc_attr($fade).'">
                                    <a href="'.esc_url($link).'" class="adv-thumb-link">'.wp_get_attachment_image($image,'full').'</a>
                                </div>
                            </div>';
                break;

            case 'adv-about':
                $html .=    '<div class="banner-adv zoom-rotate '.esc_attr($fade).'">
                                <a href="'.esc_url($link).'" class="adv-thumb-link">'.wp_get_attachment_image($image,'full').'</a>
                                <div class="banner-info adv-info-style1">';
                if(!empty($content)) $html .=   wpb_js_remove_wpautop($content, true);
                if(!empty($link)) $html .=      '<a href="'.esc_url($link).'" class="shopnow text-uppercase">'.esc_html__("Shop now","lucky").'</a>';
                $html .=        '</div>
                            </div>';
                break;

            case 'adv-home8':
                $html .=    '<div class="banner-adv zoom-rotate '.esc_attr($fade).'">
                                <a href="'.esc_url($link).'" class="adv-thumb-link">'.wp_get_attachment_image($image,'full').'</a>
                                <div class="banner-info pos-bottom white adv-info-style2">
                                   '.wpb_js_remove_wpautop($content, true).'
                                </div>
                            </div>';
                break;

            case 'countdown7':
                $html .=    '<div class="banner-megesale">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="left-mega-sale">
                                            <div class="banner-info text-uppercase text-center">
                                                '.wpb_js_remove_wpautop($content, true).'
                                            </div>
                                            <div class="hotdeal-countdown" data-date="'.esc_attr($time).'"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="right-mega-sale">
                                            <a href="'.esc_url($link).'">'.wp_get_attachment_image($image,'full').'</a>
                                        </div>
                                    </div>
                                </div>
                            </div>';
                break;

            case 'adv-home6-2':
                $html .=    '<div class="banner-adv zoom-rotate '.esc_attr($fade).'">
                                <a href="'.esc_url($link).'" class="adv-thumb-link">'.wp_get_attachment_image($image,'full').'</a>
                                <div class="banner-info text-center white adv-info-style1 pos-top">';
                if(!empty($content)) $html .=   wpb_js_remove_wpautop($content, true);
                if(!empty($link)) $html .=      '<a href="'.esc_url($link).'" class="shopnow text-uppercase">'.esc_html__("Shop now","lucky").'</a>';
                $html .=        '</div>
                            </div>';
                break;

            case 'adv-home6':
                $html .=    '<div class="banner-adv zoom-rotate '.esc_attr($fade).'">
                                <a href="'.esc_url($link).'" class="adv-thumb-link">'.wp_get_attachment_image($image,'full').'</a>
                                <div class="banner-info text-center adv-info-style1 pos-top">';
                if(!empty($content)) $html .=   wpb_js_remove_wpautop($content, true);
                if(!empty($link)) $html .=      '<a href="'.esc_url($link).'" class="shopnow text-uppercase">'.esc_html__("Shop now","lucky").'</a>';
                $html .=        '</div>
                            </div>';
                break;

            case 'adv-countdown':
                if(!empty($image)) $image = S7upf_Assets::build_css('background-image: url('.wp_get_attachment_url($image,'full').');');
                $html .=    '<div class="banner-bakground '.esc_attr($info_pos.' '.$image).'">
                                <div class="banner-info white text-uppercase">
                                    <div class="hotdeal-countdown" data-date="'.esc_attr($time).'"></div>
                                    <div class="bnbg-text">';
                if(!empty($content)) $html .=   wpb_js_remove_wpautop($content, true);
                // if(!empty($link)) $html .=      '<a href="'.esc_url($link).'" class="shopnow">'.esc_html__("Shop now","lucky").'</a>';
                 $html .=           '</div>
                                </div>
                            </div>';
                break;

            case 'adv-bgcolor':
                $html .=    '<div class="banner-adv banner-background '.esc_attr($color).'">
                                <a href="'.esc_url($link).'">
                                    <div class="banner-info white text-uppercase">';
                if(!empty($content)) $html .=   wpb_js_remove_wpautop($content, true);
                $html .=            '</div>
                                </a>
                            </div>';
                break;

            default:        
                $html .=    '<div class="banner-adv zoom-rotate '.esc_attr($color).' '.esc_attr($fade).'">
                                <a class="adv-thumb-link" href="'.esc_url($link).'">'.wp_get_attachment_image($image,'full').'</a>
                                <div class="banner-info '.esc_attr($pos_info .' '.$info_color.' '.$info_pos).' adv-info-style1">';
                if(!empty($content)) $html .=   wpb_js_remove_wpautop($content, true);
                if(!empty($link)) $html .=      '<a href="'.esc_url($link).'" class="shopnow text-uppercase">'.esc_html__("Shop now","lucky").'</a>';
                $html .=        '</div>
                            </div>';
                break;
        }
        return $html;
    }
}

stp_reg_shortcode('sv_advantage','s7upf_vc_advantage');

vc_map( array(
    "name"      => esc_html__("SV Advantage", 'lucky'),
    "base"      => "sv_advantage",
    "icon"      => "icon-st",
    "category"  => '7Up-theme',
    "params"    => array(
        array(
            "type" => "dropdown",
            "heading" => esc_html__("Style",'lucky'),
            "param_name" => "style",
            "value"     => array(
                esc_html__("Default",'lucky')   => '',
                esc_html__("Simple background",'lucky')   => 'adv-bgcolor',
                esc_html__("Adv countdown",'lucky')   => 'adv-countdown',
                esc_html__("Adv home 6",'lucky')   => 'adv-home6',
                esc_html__("Adv home 6(white)",'lucky')   => 'adv-home6-2',
                esc_html__("Countdown 7",'lucky')   => 'countdown7',
                esc_html__("Adv home 8",'lucky')   => 'adv-home8',
                esc_html__("Adv About",'lucky')   => 'adv-about',
                esc_html__("About Info",'lucky')   => 'about-info',
                esc_html__("Adv home 10",'lucky')   => 'adv-home10',
                esc_html__("Adv home 10(2)",'lucky')   => 'adv-home10-2',
                esc_html__("Adv home 11(Top close)",'lucky')   => 'adv-home11-top',
                esc_html__("Adv home 11",'lucky')   => 'adv-home11',
                esc_html__("Adv home 12",'lucky')   => 'adv-home12',
                esc_html__("Adv home 13",'lucky')   => 'adv-home13',
                esc_html__("Adv countdown 14",'lucky')   => 'adv-countdown14',
                esc_html__("Adv home 1",'lucky')   => 'adv-home1',
                )
        ),
        array(
            "type" => "textfield",
            "heading" => esc_html__("Time Countdown",'lucky'),
            "param_name" => "time",
            'description'   => esc_html__( 'Enter Time for countdown. Format is mm/dd/yyyy. Example: 12/15/2016.', 'lucky' ),
            'dependency'    => array(
                'element'   => 'style',
                'value'   => array('adv-countdown','countdown7','adv-home11-top','adv-countdown14'),
                )
        ),
        array(
            "type" => "dropdown",
            "heading" => esc_html__("Info Align",'lucky'),
            "param_name" => "info_pos",
            "value"     => array(
                esc_html__("Left",'lucky')   => 'text-left',
                esc_html__("Right",'lucky')   => 'text-right',
                esc_html__("Center",'lucky')   => 'text-center',
                esc_html__("None",'lucky')   => '',
                ),
            'dependency'    => array(
                'element'   => 'style',
                'value'   => array('adv-home13','adv-countdown','adv-home10-2','adv-home11-top','adv-home11','adv-home12','','adv-countdown14','adv-home1'),
                )
        ),
        array(
            "type" => "dropdown",
            "heading" => esc_html__("Info Position",'lucky'),
            "param_name" => "pos_info",
            "value"     => array(
                esc_html__("None",'lucky')   => '',
                esc_html__("Top",'lucky')   => 'pos-top',
                esc_html__("Bottom",'lucky')   => 'pos-bottom',
                esc_html__("Left",'lucky')   => 'pos-left',
                esc_html__("Right",'lucky')   => 'pos-right',
                ),
            'dependency'    => array(
                'element'   => 'style',
                'value'   => array('adv-home13','adv-home10','adv-home11','adv-home12','','adv-countdown14','adv-home1'),
                )
        ),
        array(
            "type"          => "colorpicker",
            "heading"       => esc_html__("Color",'lucky'),
            "param_name"    => "color",
        ),
        array(
            "type" => "attach_image",
            "heading" => esc_html__("Image",'lucky'),
            "param_name" => "image",
        ),
        array(
            "type" => "textfield",
            "heading" => esc_html__("Link",'lucky'),
            "param_name" => "link",
        ),        
        array(
            "type" => "dropdown",
            "heading" => esc_html__("Amination",'lucky'),
            "param_name" => "fade",
            "value"     => array(
                esc_html__("No",'lucky')   => '',
                esc_html__("Fade out in",'lucky')   => 'fade-out-in',
                esc_html__("Overlay Image",'lucky')   => 'overlay-image',
                esc_html__("Zoom Image",'lucky')   => 'banner-zoom5',
                ),
        ),
        array(
            "type" => "dropdown",
            "heading" => esc_html__("Info color",'lucky'),
            "param_name" => "info_color",
            "value"     => array(
                esc_html__("Default",'lucky')   => '',
                esc_html__("White",'lucky')   => 'white',
                ),
            'dependency'    => array(
                'element'   => 'style',
                'value'   => array('adv-home13','adv-home10','adv-home10-2','adv-home11-top','adv-home11','adv-home12','','adv-countdown14','adv-home1'),
                )
        ),
        array(
            "type" => "textarea_html",
            "holder" => "div",
            "heading" => esc_html__("Content",'lucky'),
            "param_name" => "content",
        ),
    )
));