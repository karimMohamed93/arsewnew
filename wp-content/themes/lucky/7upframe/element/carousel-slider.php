<?php
/**
 * Created by Sublime text 2.
 * User: thanhhiep992
 * Date: 31/08/15
 * Time: 10:00 AM
 */
/************************************Main Carousel*************************************/
if(!function_exists('s7upf_vc_slide_carousel'))
{
    function s7upf_vc_slide_carousel($attr, $content = false)
    {
        $html = $css_class = '';
        extract(shortcode_atts(array(
            'item'      => '1',
            'speed'     => '',
            'itemres'   => '',
            'nav_slider'=> 'nav-hidden',
            'animation' => '',
            'custom_css' => '',
            'banner_bg' => '',
            'title' => '',
        ),$attr));
        if(!empty($custom_css)) $css_class = vc_shortcode_custom_css_class( $custom_css );
        if(!empty($title)) $html .= '<div class="contain-slider14"><div class="title-box1 style2">
                                        <h2 class="title30"><span>'.esc_html($title).'</span></h2>
                                    </div>';
            $html .=    '<div class="banner-slider '.esc_attr($nav_slider.' '.$banner_bg).'">';
            $html .=        '<div class="wrap-item sv-slider '.$css_class.'" data-item="'.$item.'" data-speed="'.$speed.'" data-itemres="'.$itemres.'" data-animation="'.$animation.'" data-nav="'.$nav_slider.'" data-prev="'.esc_attr__("Prev","lucky").'" data-next="'.esc_attr__("Next","lucky").'">';
            $html .=            wpb_js_remove_wpautop($content, false);
            $html .=        '</div>';
            $html .=    '</div>';
            if(!empty($title)) $html .= '</div>';
        return $html;
    }
}
stp_reg_shortcode('slide_carousel','s7upf_vc_slide_carousel');
vc_map(
    array(
        'name'     => esc_html__( 'Carousel Slider', 'lucky' ),
        'base'     => 'slide_carousel',
        'category' => esc_html__( '7Up-theme', 'lucky' ),
        'icon'     => 'icon-st',
        'as_parent' => array( 'only' => 'vc_column_text,vc_single_image,slide_banner_item,slide_testimonial_item,slide_service_item,slide_adv_item' ),
        'content_element' => true,
        'js_view' => 'VcColumnView',
        'params'   => array(                       
            array(
                'heading'     => esc_html__( 'Item slider display', 'lucky' ),
                'type'        => 'textfield',
                'description' => esc_html__( 'Enter number of item. Default is 1.', 'lucky' ),
                'param_name'  => 'item',
            ),
            array(
                'type'        => 'dropdown',
                'heading'     => esc_html__( 'Image style', 'lucky' ),
                'param_name'  => 'banner_bg',
                'value'       => array(
                    esc_html__( 'Default', 'lucky' )                  => '',
                    esc_html__( 'Banner Background', 'lucky' )      => 'bg-slider',
                ),
            ),
            array(
                'heading'     => esc_html__( 'Speed', 'lucky' ),
                'type'        => 'textfield',
                'description' => esc_html__( 'Enter time slider go to next item. Unit (ms). Example 5000. If empty this field autoPlay is false.', 'lucky' ),
                'param_name'  => 'speed',
            ),
            array(
                'type'        => 'dropdown',
                'heading'     => esc_html__( 'Navigation style', 'lucky' ),
                'param_name'  => 'nav_slider',
                'value'       => array(
                    esc_html__( 'Hidden', 'lucky' )                  => 'nav-hidden',
                    esc_html__( 'Default Navigation', 'lucky' )      => 'banner-slider',
                    esc_html__( 'Navigation 2', 'lucky' )      => 'banner-slider2',
                    esc_html__( 'Pagination 3', 'lucky' )      => 'banner-slider3',
                    esc_html__( 'Pagination Testimonial', 'lucky' )      => 'about-testimo-slider',
                    esc_html__( 'Navigation 6', 'lucky' )      => 'about-service-slider',
                    esc_html__( 'Navigation 9', 'lucky' )      => 'banner-slider9 long-arrow',
                    esc_html__( 'Navigation 9(outer)', 'lucky' )      => 'adv-slider9 long-arrow',
                    esc_html__( 'Navigation 10', 'lucky' )      => 'banner-slider10',
                    esc_html__( 'Pagination 11', 'lucky' )      => 'banner-slider2 banner-slider11',
                    esc_html__( 'Navigation 11', 'lucky' )      => 'product-type-slider long-arrow',
                    esc_html__( 'Navigation 13', 'lucky' )      => 'banner-slider13',
                    esc_html__( 'Navigation 14', 'lucky' )      => 'hotcat-slider14 arrow-style14',
                ),
            ),
            array(
                'heading'     => esc_html__( 'Title', 'lucky' ),
                'type'        => 'textfield',
                'param_name'  => 'title',
                'dependency'    => array(
                    'element'   => 'nav_slider',
                    'value'   => 'hotcat-slider14 arrow-style14',
                    )
            ),
            array(
                'heading'     => esc_html__( 'Custom Item', 'lucky' ),
                'type'        => 'textfield',
                'description'   => esc_html__( 'Enter item for screen width(px) format is width:value and separate values by ",". Example is 0:2,600:3,1000:4. Default is auto.', 'lucky' ),
                'param_name'  => 'itemres',
            ),
            array(
                'type'        => 'dropdown',
                'heading'     => esc_html__( 'Slider Animation', 'lucky' ),
                'param_name'  => 'animation',
                'value'       => array(
                    esc_html__( 'None', 'lucky' )        => '',
                    esc_html__( 'Fade', 'lucky' )        => 'fade',
                    esc_html__( 'BackSlide', 'lucky' )   => 'backSlide',
                    esc_html__( 'GoDown', 'lucky' )      => 'goDown',
                    esc_html__( 'FadeUp', 'lucky' )      => 'fadeUp',
                    )
            ),
            array(
                "type"          => "css_editor",
                "heading"       => esc_html__("Custom Block",'lucky'),
                "param_name"    => "custom_css",
                'group'         => esc_html__('Advanced','lucky')
            )
        )
    )
);

/*******************************************END MAIN*****************************************/


/**************************************BEGIN ITEM************************************/
//Banner item Frontend
if(!function_exists('s7upf_vc_slide_banner_item'))
{
    function s7upf_vc_slide_banner_item($attr, $content = false)
    {
        $html = $view_html = $view_html2 = '';
        extract(shortcode_atts(array(
            'style'         => '',
            'image'         => '',
            'image_inner'         => '',
            'link'          => '',            
            'thumb_animation' => '',
            'info_animation' => '',
            'info_style' => 'black',
        ),$attr));
        if(!empty($image)){
            $thumb_class = $info_class = '';
            if(!empty($thumb_animation)) $thumb_class = 'animated';
            if(!empty($info_animation)) $info_class = 'animated';
            switch ($style) {
                case 'home-2':
                    $html .=    '<div class="item-slider '.esc_attr($style).'">
                                    <div class="banner-thumb">
                                        <a href="'.esc_url($link).'"><img src="'.wp_get_attachment_url($image).'" alt=""/></a>
                                    </div>
                                    <div class="banner-info text-uppercase '.esc_attr($info_style).'">
                                        <div class="container">
                                            <div class="banner-info-text '.esc_attr($info_class).'" data-anim-type="'.esc_attr($info_animation).'">
                                                '.wpb_js_remove_wpautop($content, true).'
                                            </div>
                                            <div class="banner-info-image '.esc_attr($thumb_class).'" data-anim-type="'.esc_attr($thumb_animation).'">
                                                '.wp_get_attachment_image($image_inner,'full').'
                                            </div>
                                        </div>
                                    </div>
                                </div>';
                    break;
                
                default:
                    $html .=    '<div class="item-slider '.esc_attr($style).'">
                                    <div class="banner-thumb '.esc_attr($thumb_class).'" data-anim-type="'.esc_attr($thumb_animation).'">
                                        <a href="'.esc_url($link).'"><img src="'.wp_get_attachment_url($image).'" alt=""/></a>
                                    </div>';
                    if(!empty($content)){
                    $html .=        '<div class="banner-info '.esc_attr($info_style).' text-center text-uppercase '.esc_attr($info_class).'" data-anim-type="'.esc_attr($info_animation).'">
                                        '.wpb_js_remove_wpautop($content, true).'
                                    </div>';
                                }
                    $html .=    '</div>';
                    break;
            }            
        }
        return $html;
    }
}
stp_reg_shortcode('slide_banner_item','s7upf_vc_slide_banner_item');

// Banner item
vc_map(
    array(
        'name'     => esc_html__( 'Banner Item', 'lucky' ),
        'base'     => 'slide_banner_item',
        'icon'     => 'icon-st',
        'content_element' => true,
        'as_child' => array('only' => 'slide_carousel'),
        'params'   => array(
            array(
                'type'        => 'dropdown',
                'heading'     => esc_html__( 'Style', 'lucky' ),
                'param_name'  => 'style',
                'value'       => array(
                    esc_html__( 'Default', 'lucky' ) => '',
                    esc_html__( 'Home 2', 'lucky' ) => 'home-2',
                    esc_html__( 'Home 6', 'lucky' ) => 'home-6',
                    )
            ),            
            array(
                'type'        => 'attach_image',
                'heading'     => esc_html__( 'Image', 'lucky' ),
                'param_name'  => 'image',
            ),
            array(
                'type'        => 'attach_image',
                'heading'     => esc_html__( 'Image Inner', 'lucky' ),
                'param_name'  => 'image_inner',
                'dependency'  => array(
                    'element'   => 'style',
                    'value'     => 'home-2',
                    )
            ),
            array(
                'type'        => 'textfield',
                'heading'     => esc_html__( 'Link Banner', 'lucky' ),
                'param_name'  => 'link',
            ),            
            array(
                    'type' => 'dropdown',
                    'heading' => esc_html__( 'Image Animation', 'lucky' ),
                    'param_name' => 'thumb_animation',
                    'value' => s7upf_get_list_animation()
                ),
            array(
                    'type' => 'dropdown',
                    'heading' => esc_html__( 'Info Animation', 'lucky' ),
                    'param_name' => 'info_animation',
                    'value' => s7upf_get_list_animation()
                ),
            array(
                    'type' => 'dropdown',
                    'heading' => esc_html__( 'Info Style', 'lucky' ),
                    'param_name' => 'info_style',
                    'value' => array(
                        esc_html__( 'Black', 'lucky' )     => 'black',
                        esc_html__( 'White', 'lucky' )     => 'white',
                        )
                ),
            array(
                "type" => "textarea_html",
                "holder" => "div",
                "heading" => esc_html__("Content",'lucky'),
                "param_name" => "content",
            ),
        )
    )
);

/**************************************END ITEM************************************/

/**************************************BEGIN ITEM************************************/
//Banner item Frontend
if(!function_exists('s7upf_vc_slide_testimonial_item'))
{
    function s7upf_vc_slide_testimonial_item($attr, $content = false)
    {
        $html = $view_html = $view_html2 = '';
        extract(shortcode_atts(array(
            'style'         => '',
            'image'         => '',
            'name'          => '',
            'position'      => '',            
            'des'           => '',
            'link'          => '',
        ),$attr));
        switch ($style) {
            case 'image-left':
                $html .=    '<div class="item-test-about white table">
                                <div class="about-testimo-left text-center">
                                    <div class="test-about-avar">
                                        <a href="'.esc_url($link).'">'.wp_get_attachment_image($image,'full').'</a>
                                    </div>
                                    <h3 class="title14">'.esc_html($name).'</h3>
                                    <span'.esc_html($position).'</span>
                                </div>
                                <div class="about-testimo-right">
                                    <p>'.esc_html($des).'</p>
                                </div>
                            </div>';
                break;
            
            default:
                $html .=    '<div class="item-test-about white text-center">
                                <div class="test-about-avar">
                                    <a href="'.esc_url($link).'">'.wp_get_attachment_image($image,'full').'</a>
                                </div>
                                <h3 class="title14">'.esc_html($name).'</h3>
                                <span>'.esc_html($position).'</span>
                                <p>'.esc_html($des).' </p>
                            </div>';
                break;
        }        
        return $html;
    }
}
stp_reg_shortcode('slide_testimonial_item','s7upf_vc_slide_testimonial_item');

// Banner item
vc_map(
    array(
        'name'     => esc_html__( 'Testimonial Item', 'lucky' ),
        'base'     => 'slide_testimonial_item',
        'icon'     => 'icon-st',
        'content_element' => true,
        'as_child' => array('only' => 'slide_carousel'),
        'params'   => array(
            array(
                'type'        => 'dropdown',
                'heading'     => esc_html__( 'Style', 'lucky' ),
                'param_name'  => 'style',
                'value'       => array(
                    esc_html__( 'Default', 'lucky' ) => '',
                    esc_html__( 'Left style', 'lucky' ) => 'image-left',
                    )
            ),            
            array(
                'type'        => 'attach_image',
                'heading'     => esc_html__( 'Image', 'lucky' ),
                'param_name'  => 'image',
            ),
            array(
                'type'        => 'textfield',
                "holder"        => "h3",
                'heading'     => esc_html__( 'Name', 'lucky' ),
                'param_name'  => 'name',
            ),

            array(
                'type'        => 'textfield',
                'heading'     => esc_html__( 'Position', 'lucky' ),
                'param_name'  => 'position',
            ),
            array(
                'type'        => 'textfield',
                'heading'     => esc_html__( 'Link', 'lucky' ),
                'param_name'  => 'link',
            ),  
            array(
                "type"          => "textarea",
                "holder"        => "p",
                "heading"       => esc_html__("Description",'lucky'),
                "param_name"    => "des",
            ),
        )
    )
);

/**************************************END ITEM************************************/

/**************************************BEGIN ITEM************************************/
//Banner item Frontend
if(!function_exists('s7upf_vc_slide_service_item'))
{
    function s7upf_vc_slide_service_item($attr, $content = false)
    {
        $html = $view_html = $view_html2 = '';
        extract(shortcode_atts(array(
            'style'         => '',
            'icon'          => '',
            'title'         => '',          
            'des'           => '',
            'link'          => '',
        ),$attr));
        $html .=    '<div class="item-service2 table">
                        <div class="service-icon"><a href="'.esc_url($link).'"><i class="fa '.esc_attr($icon).'" aria-hidden="true"></i></a></div>
                        <div class="service-info">
                            <p class="text-uppercase">'.esc_html($title).'</p>
                            <p class="desc">'.esc_html($des).'</p>
                        </div>
                    </div>';
        return $html;
    }
}
stp_reg_shortcode('slide_service_item','s7upf_vc_slide_service_item');

// Banner item
vc_map(
    array(
        'name'     => esc_html__( 'Service Item', 'lucky' ),
        'base'     => 'slide_service_item',
        'icon'     => 'icon-st',
        'content_element' => true,
        'as_child' => array('only' => 'slide_carousel'),
        'params'   => array(
            array(
                'type'        => 'dropdown',
                'heading'     => esc_html__( 'Style', 'lucky' ),
                'param_name'  => 'style',
                'value'       => array(
                    esc_html__( 'Default', 'lucky' ) => '',
                    )
            ),            
            array(
                'type'        => 'textfield',
                'heading'     => esc_html__( 'Icon', 'lucky' ),
                'param_name'  => 'icon',
                'edit_field_class'=>'vc_col-sm-12 vc_column sv_iconpicker',
            ),
            array(
                'type'        => 'textfield',
                "holder"        => "h3",
                'heading'     => esc_html__( 'Title', 'lucky' ),
                'param_name'  => 'title',
            ),
            array(
                'type'        => 'textfield',
                'heading'     => esc_html__( 'Link', 'lucky' ),
                'param_name'  => 'link',
            ),  
            array(
                "type"          => "textarea",
                "holder"        => "p",
                "heading"       => esc_html__("Description",'lucky'),
                "param_name"    => "des",
            ),
        )
    )
);

/**************************************END ITEM************************************/

/**************************************BEGIN ITEM************************************/
//Banner item Frontend
if(!function_exists('s7upf_vc_slide_adv_item'))
{
    function s7upf_vc_slide_adv_item($attr, $content = false)
    {
        $html = $view_html = $view_html2 = '';
        extract(shortcode_atts(array(
            'style'         => 'default',
            'image'         => '',
            'title'         => '',
            'des'           => '',
            'link'          => '',
        ),$attr));
        switch ($style) {
            case 'home14':
                $html .=    '<div class="item-hotcat14">
                                <div class="hotcat-thumb">
                                    <a href="'.esc_url($link).'">'.wp_get_attachment_image($image,'full').'</a>
                                </div>
                                <div class="hotcat-info text-uppercase">
                                    <h3 class="title18"><a href="'.esc_url($link).'">'.esc_html($title).'</a></h3>
                                    <a href="'.esc_url($link).'" class="btn-rect">'.esc_html__("brow store","lucky").'</a>
                                </div>
                            </div>';
                break;
            
            default:
                $html .=    '<div class="item">
                                <div class="item-cat-hover-dir box-hover-dir">
                                    '.wp_get_attachment_image($image,'full').'
                                    <div class="content-cat-hover-dir">
                                        <div class="inner-cat-hover-dir">
                                            <div class="text-hover-dir white">
                                                <h2 class="title18">'.esc_html($title).'</h2>
                                                <p>'.esc_html($des).'</p>
                                                <a href="'.esc_url($link).'" class="btn-rect text-uppercase">'.esc_html__("brow store","lucky").'</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>';
                break;
        }        
        return $html;
    }
}
stp_reg_shortcode('slide_adv_item','s7upf_vc_slide_adv_item');

// Banner item
vc_map(
    array(
        'name'     => esc_html__( 'Advantage Item', 'lucky' ),
        'base'     => 'slide_adv_item',
        'icon'     => 'icon-st',
        'content_element' => true,
        'as_child' => array('only' => 'slide_carousel'),
        'params'   => array( 
            array(
                'type'        => 'dropdown',
                'heading'     => esc_html__( 'Style', 'lucky' ),
                'param_name'  => 'style',
                'value'       => array(
                    esc_html__( 'Default', 'lucky' )  => 'default',
                    esc_html__( 'Home 14', 'lucky' )  => 'home14',
                    )
            ),
            array(
                'type'        => 'attach_image',
                'heading'     => esc_html__( 'Image', 'lucky' ),
                'param_name'  => 'image',
            ),
            array(
                'type'        => 'textfield',
                "holder"        => "h3",
                'heading'     => esc_html__( 'Title', 'lucky' ),
                'param_name'  => 'title',
            ),
            array(
                'type'        => 'textfield',
                'heading'     => esc_html__( 'Link', 'lucky' ),
                'param_name'  => 'link',
            ),  
            array(
                "type"          => "textarea",
                "holder"        => "p",
                "heading"       => esc_html__("Description",'lucky'),
                "param_name"    => "des",
                'dependency'  => array(
                    'element'   => 'style',
                    'value'     => 'default',
                    )
            ),
        )
    )
);

/**************************************END ITEM************************************/

//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_Slide_Carousel extends WPBakeryShortCodesContainer {}
}
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_Slide_Banner_Item extends WPBakeryShortCode {}
    class WPBakeryShortCode_Slide_Testimonial_Item extends WPBakeryShortCode {}
    class WPBakeryShortCode_Slide_Service_Item extends WPBakeryShortCode {}
    class WPBakeryShortCode_Slide_Adv_Item extends WPBakeryShortCode {}
}